#/bin/sh

if [ "" != "$(docker ps -q)" ] ; then 
    docker kill $(docker ps -q)
fi

docker-compose -f docker-compose.$1.yml up --build --force-recreate -d

if [ -f "show-log.$1.sh" ] ; then
    ./show-log.$1.sh $1
fi