#!/bin/sh

journalctl -o json -f | jq '.|select(.CONTAINER_NAME!=null)'
