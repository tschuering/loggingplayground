#!/bin/sh -x
IN=$1
shift
if [ "" = "$1" ] ; then
    OUT=$(basename $IN .md)
else
    OUT=$1
fi
jq --slurp --raw-input '{"text": "\(.)", "mode": "markdown"}' < "/data/$IN" | curl -s --data @- https://api.github.com/markdown > "/data/$OUT"