# Central Log Management (Demo)

## Preliminaries

    ssh hgdeclfr001724
    ./demo.sh 0

## 0 - (local) jsonfile

[nginx](http://hgdeclfr001724:10080)

[Docker "only"](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.0.yml?at=master&fileviewer=file-view-default)

## 13 - Secure'n'Safe From service to fluentd to fluentd to graylog

[Encrypted forwarding ...](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.13.yml?at=master&fileviewer=file-view-default)

demo/demodemo

[graylog](https://log.haufe.io/streams/582dca9377671c0006d6a5c1/search)

## X - Windows, anyone?

[Using nxlog](http://docs.fluentd.org/articles/windows#set-up-nxlog-on-windows)

## More samples / setups

### 1 - (local) journald

[Why not use the systems log?](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.1.yml?at=master&fileviewer=file-view-default)

### 2 - Fluentd / log.io   

[Use fluent ... best in a container](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.2.yml?at=master&fileviewer=file-view-default)

[log.io](http://hgdeclfr001724:28778)

### 3 - Fluentd / Elasticsearch /Kibana   

[You could do your "own" log-stack ...](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.3.yml?at=master&fileviewer=file-view-default)

[elasticsearch](http://hgdeclfr001724:9200)
[kibana](http://hgdeclfr001724:5601)

### 10 - Direct sending from local to graylog Fluentd / Graylog

[Check that graylog works ...](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.10.yml?at=master&fileviewer=file-view-default)

### 11 - From service to fluentd to fluentd to graylog

[Unencrypted forwarding ...](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.11.yml?at=master&fileviewer=file-view-default)

### 12 - From service to fluentd to fluentd to graylog

[Encrypted forwarding ...](https://bitbucket.org/tschuering/loggingplayground/src/master/docker-compose.12.yml?at=master&fileviewer=file-view-default)


## Worth your time ...

[Logging-Playground](https://bitbucket.org/tschuering/loggingplayground)

[Fluentd-Fwd](https://bitbucket.org/haufegroup/hgg-ida.fluentd-fwd)

[Fluentd-Fwd-Readme](https://bitbucket.org/haufegroup/hgg-ida.fluentd-fwd/src/master/README.haufe.md?at=master)
