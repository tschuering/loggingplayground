#!/bin/sh

set -x 

# docker-compose -f docker-compose.$1.yml logs -f

journalctl -o json -f | jq '.|select(.CONTAINER_NAME!=null)'

